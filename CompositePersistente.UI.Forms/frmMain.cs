﻿using CompositePersistente.BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompositePersistente.UI.Forms
{
    public partial class frmMain : Form
    {

        UsuariosBLL bllUsuarios;
        PermisosBLL bllPermisos;
        BLLSesion bllSesion;

        private void usuario_Click(object sender, EventArgs e)
        {


            Usuario u =(Usuario)((ToolStripMenuItem)sender).Tag;

            bllSesion.Login(u);

            this.lblUsuario.Text = u.Nombre;


            //SimuladorSesion.GetInstance.Login(u);
            // Do something
            ValidarPermisos();
        }


        void ValidarPermisos()
        {
            if (SimuladorSesion.GetInstance.IsLoggedIn())
            {
                this.mnuEjemplo.Visible = SimuladorSesion.GetInstance.IsInRole(TipoPermiso.PuedeHacerF);
                this.mnuA.Enabled = SimuladorSesion.GetInstance.IsInRole(TipoPermiso.PuedeHacerA);
                this.mnuB.Enabled = SimuladorSesion.GetInstance.IsInRole(TipoPermiso.PuedeHacerB);
                this.mnuC.Enabled = SimuladorSesion.GetInstance.IsInRole(TipoPermiso.PuedeHacerC);
                this.mnuD.Enabled = SimuladorSesion.GetInstance.IsInRole(TipoPermiso.PuedeHacerD);
                this.mnuE.Enabled = SimuladorSesion.GetInstance.IsInRole(TipoPermiso.PuedeHacerE);
                this.mnuG.Enabled = SimuladorSesion.GetInstance.IsInRole(TipoPermiso.PuedeHacerG);

            }
            else
            {
                this.mnuEjemplo.Enabled = false;
                this.mnuA.Enabled = false;
                this.mnuB.Enabled = false;
                this.mnuC.Enabled = false;
                this.mnuD.Enabled = false;
                this.mnuE.Enabled = false;
                this.mnuG.Enabled = false;

            }
        }


        public frmMain()
        {
            InitializeComponent();

            bllPermisos = new PermisosBLL();
            bllUsuarios = new UsuariosBLL();
            bllSesion = new BLLSesion();
            var usuarios = bllUsuarios.GetAll();
            foreach (var item in usuarios)
            {
                ToolStripMenuItem t = new ToolStripMenuItem(item.Nombre);
                t.Tag = item;
                bllPermisos.FillUserComponents(item);

                t.Click += usuario_Click;
                mnuSimularSesion.DropDownItems.Add(t);
            }

            ValidarPermisos();
        }

      

        private void SeguridadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPatentesFamilias frm = new frmPatentesFamilias();
            frm.MdiParent = this;
            frm.Show();
        }

        private void UsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUsuarios frm = new frmUsuarios();
            frm.MdiParent = this;
            frm.Show();
        }

        private void FormNuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {


            if (SimuladorSesion.GetInstance.IsInRole(TipoPermiso.PuedeHacerG))
            {
                frmNuevo frm = new frmNuevo();
                frm.MdiParent = this;
                frm.Show();
            }
            else
            {
                MessageBox.Show("no tiene permisos");
            }
        
        }

        private void VentasToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                BllVentas bll = new BllVentas();
                bll.Facturar();
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
                
            }

        }
    }
}
